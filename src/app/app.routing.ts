import { Routes, RouterModule }   from '@angular/router';
import { ProfileComponent }       from './admin/profile/profile.component';
//import { ProfileFormComponent }   from './profile-form/profile-form.component';
//import { LoginComponent }         from './admin/login/login.component';
//import { RegisterComponent }      from './admin/register/register.component';
import { MainPageComponent }      from './main-page/main-page.component';
import { GameMainPageComponent }  from './game/game-main-page/game-main-page.component';
import { GameHomePageComponent }  from './game/game-home-page/game-home-page.component';
import { PageNotFoundComponent }  from './not-found.component';

import { CanDeactivateGuard } from './can-deactivate-guard.service';
import { PreloadSelectedModules } from './selective-preload-strategy';
import { AuthenticationService } from './services/authentication.service';
import { AboutPageComponent } from './about-page/about-page.component';
import { ContactPageComponent } from './contact-page/contact-page.component';



const appRoutes: Routes = [
  {
    path: '',
    //redirectTo: '/game-home-page',
    redirectTo: '/main',
    pathMatch: 'full'
  },
  {
    path:'admin',
    loadChildren:'app/admin/admin.module#AdminModule'
  },
  {
    path: 'game-main-Page',
    loadChildren:'app/game/games.module#GamesModule'
  },
  {
    path: 'game-home-page',
    component: GameHomePageComponent
  },
  {
    path: 'main',
    component: MainPageComponent
  },
  {
    path: 'about',
    component: AboutPageComponent
  },
  {
    path: 'contact',
    component: ContactPageComponent
  },
  {
    path:'**',
    component:PageNotFoundComponent
  }


];

export const routing = RouterModule.forRoot(appRoutes,
                                            {preloadingStrategy:PreloadSelectedModules}
                                            );
//export const routedComponents = [ProfileComponent];
