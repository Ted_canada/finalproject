import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule,XHRBackend, RequestOptions  } from '@angular/http';
import { FlexLayoutModule } from '@angular/flex-layout';
//import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
//import { LoginComponent } from './admin/login/login.component';
//import { RegisterComponent } from './admin/register/register.component';
//import { ProfileComponent } from './admin/profile/profile.component';


import { PageNotFoundComponent }  from './not-found.component';

import { InterceptorService } from 'ng2-interceptors';
import { ServerURLInterceptor } from './interceptor';


//import { routing, routedComponents } from './app.routing';
import { routing } from './app.routing';
import { RestService } from './services/rest.service';
//import { AuthService }      from './services/auth.service';
import { AlertComponent } from './alert/alert.component';
import { AuthenticationService } from './services/authentication.service';
import { EmailVerifyService } from './services/email-verify.service';
import {ProfilePicService }     from './services/profile-pic.service';

import { GamesModule }      from './game/games.module';
import { AdminModule }      from './admin/admin.module';
import { AdminComponent }      from './admin/admin.component';
import { LoginRoutingModule }   from './admin/login-routing.module';
import { MainPageComponent } from './main-page/main-page.component';
import { GameMainPageComponent } from './game/game-main-page/game-main-page.component';
import { GameHomePageComponent } from './game/game-home-page/game-home-page.component';

import { CanDeactivateGuard } from './can-deactivate-guard.service';
import { PreloadSelectedModules } from './selective-preload-strategy';
import { AboutPageComponent } from './about-page/about-page.component';
import { ContactPageComponent } from './contact-page/contact-page.component';



//import { Angular2SocialLoginModule } from "angular2-social-login";
export function interceptorFactory(xhrBackend: XHRBackend, requestOptions: RequestOptions, serverURLInterceptor:ServerURLInterceptor){ // Add it here
  let service = new InterceptorService(xhrBackend, requestOptions);
  service.addInterceptor(serverURLInterceptor); // Add it here
  return service;
}

@NgModule({
  declarations: [
    AppComponent,
    MainPageComponent,
    PageNotFoundComponent,
    AlertComponent,
    GameHomePageComponent,
    AboutPageComponent,
    ContactPageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AdminModule,
    GamesModule,
    HttpModule,
    FlexLayoutModule,
    //LoginRoutingModule,
    routing
  ],
  providers: [RestService,
              ServerURLInterceptor,
              CanDeactivateGuard,
              PreloadSelectedModules,
              AuthenticationService,
              EmailVerifyService,
              ProfilePicService,
              {
               provide: InterceptorService,
               useFactory: interceptorFactory,
               deps: [XHRBackend, RequestOptions, ServerURLInterceptor] }],
  bootstrap: [AppComponent]

})
export class AppModule { }
