import { Component, OnInit }  from '@angular/core';
import { Pipe }               from '@angular/core';
import { Router }             from '@angular/router';
import { Headers,RequestOptions, URLSearchParams }    from '@angular/http';



import {profile}      from '../../profile';
import {RestService, AlertService,AuthenticationService } from '../../servicesForAccount';
import { EmailVerifyService }     from '../../services/email-verify.service';
//import { AdminService }   from '../admin.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [RestService, AlertService,AuthenticationService,EmailVerifyService]
})


export class RegisterComponent implements OnInit {

  errorMessage: string;
  profiles:profile[] =[];
  model: any = {};
  mode = 'Observable';
  loading = false;
  token = '';
  //bEmailVerified = false;
  test = true;

  //private default_client_url = 'http://localhost:8080/api/profiles';
  private default_client_url = 'https://finalproject-kcho33.c9users.io:8080/api/profiles';
  private current_url;


  constructor(private restService: RestService,
              private alertService: AlertService,
              private router: Router,
              private authService:AuthenticationService,
              //private adminService:AdminService;
              private emailverifyService : EmailVerifyService) {
    this.setURL( this.default_client_url );
    this.restService.setURL( this.getURL() );
    this.token = 'currentUser';
    this.model.bEmailVerified = false;

  }
  ngOnInit() {
      this.getProfiles();
  }

  // get current url for backend-end server to send a post request
  getURL(): string{
    return this.current_url;
  }

  // save current url for backend-end server to send a post request
  setURL( url : string ){
    this.current_url  = url;
  }
  
  // get all profiles
  getProfiles() {
    this.setURL( this.default_client_url );
    this.restService.setURL( this.getURL() );
    let dbgVal:profile[] =[];

    this.restService.GetRest()
                    .subscribe(
                      profiles  => {
                        this.profiles = profiles;
                        this.getValue(this.profiles);
                      },
                      error     => this.errorMessage  = <any>error );

    //console.log('Dbg.....' + dbgVal);


  }

  // get current profiles that is existed
  getValue(profiles:any){
    this.profiles = profiles;
    console.log('subscirbe : ' + this.profiles);
    for( let profile of this.profiles){
      console.log('profile ID :' + profile.username );
    }
  }

  // register new account to DB
  addProfile(){
    this.loading = true;

    //let add_url = 'http://localhost:8080/api/profiles';
    let add_url = 'https://finalproject-kcho33.c9users.io:8080/api/profiles';
    this.setURL( add_url );
    this.restService.setURL( this.getURL() );

    var headers = new Headers( {'Content-Type': 'application/x-www-form-urlencoded'});
    //let currentItem = JSON.parse(localStorage.getItem(this.token));
    headers.append( 'Authorization', this.token );

    let options = new RequestOptions( {headers:headers});
    let data    = new URLSearchParams();
    data.append('username'  , this.model.username);
    data.append('password'  , this.model.password);
    data.append('firstname' , this.model.firstname);
    data.append('lastname'  , this.model.lastname);
    data.append('age'       , this.model.age);
    data.append('gended'    , this.model.gended);
    data.append('email'     , this.model.email);
    data.append('picture'   , this.model.picture);

    this.restService.CreateRest(data, options)
                    .subscribe(
                    data => {
                      this.alertService.success(
                        'Registration successful', true);
                      this.router.navigate(['/profiles']);
                    },
                    error => {
                      this.alertService.error(error);
                      this.loading = false;
                    });
  }

  // sending a email to verify
  verifyEmail(){
    let email_addr = this.model.email;
    this.emailverifyService.verifyEmail(email_addr)
                           .subscribe((status:any) => {
                              this.model.bEmailVerified = status;
                              return true;
                            });
  }



}
