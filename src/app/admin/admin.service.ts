import { Injectable } from '@angular/core';

import { Http, Response, Headers, RequestOptions } from '@angular/http';
//import { User }           from './profile.interface';
import {Observable} from 'rxjs/Rx';
import { InterceptorService } from 'ng2-interceptors';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class AdminService {

  username = '';

  //private commentsUrl = 'http://localhost:8090/api/profile';
  private commentsUrl = 'https://finalproject-kcho33.c9users.io:8081/api/profile';
  
  

  // Resolve HTTP using the constructor
  constructor (private _http: InterceptorService) {}


  getProfile() : Observable<any> {
       // ...using get request
       return this._http.get(this.commentsUrl)
                      // ...and calling .json() on the response to return data
                       .map((res:Response) => res.json())
                       //...errors if any
                       .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
   }

   setUsername( username: string ){
     this.username = username;
   }


}
