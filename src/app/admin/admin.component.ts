import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin',
  template:  `
  <!--<nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">TEDs Game</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <form class="navbar-form navbar-right">
              <button routerLink = "/login" type="submit" class="btn btn-success">Sign in</button>
              <button routerLink = "/register" type="submit" class="btn btn-success">Sign up</button>
            </form>
          </div>
        </div>
      </nav>-->
    <router-outlet></router-outlet>
  `
  //styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
