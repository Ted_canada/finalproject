import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AdminComponent }           from './admin.component';
import { AdminRoutingModule }       from './admin-routing.module';
import { ProfileComponent }         from './profile/profile.component';
import { LoginComponent }           from './login/login.component';
import { RegisterComponent }        from './register/register.component';
import { AdminService }             from './admin.service';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AdminRoutingModule
  ],
  declarations: [
    AdminComponent,
    ProfileComponent,
    LoginComponent,
    RegisterComponent
  ],
  providers: [ AdminService ]
})
export class AdminModule {}
