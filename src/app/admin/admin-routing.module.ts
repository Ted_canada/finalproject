import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminComponent }           from './admin.component';
import { LoginComponent }              from './login/login.component';
import { RegisterComponent }           from './register/register.component';
import { AuthenticationService }                from '../services/authentication.service';
import { ProfileComponent }            from './profile/profile.component';


// register routing path
const adminRoutes: Routes = [
  {
    path: '',
    component: AdminComponent,
    canActivate: [AuthenticationService],
    children: [
      {
        path: '',
        canActivateChild: [AuthenticationService],
        children: [
          { path: '', component: AdminComponent }

        ],

      }
    ]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'game-main-Page',
    loadChildren:'app/game/games.module#GamesModule'
  },
  {
    path:'profiles',
    component : ProfileComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(adminRoutes)
  ],
  exports: [
    RouterModule
  ],
  providers: [ AuthenticationService ]
})
export class AdminRoutingModule {}
