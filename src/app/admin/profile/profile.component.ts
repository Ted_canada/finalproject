import { Component, OnInit } from '@angular/core';
import { Router }             from '@angular/router';
import { GameService }        from '../../game/game.service';
import {RestService, AlertService,AuthenticationService } from '../../servicesForAccount';
import { Headers,RequestOptions, URLSearchParams }    from '@angular/http';

import {ProfilePicService }     from '../../services/profile-pic.service';

import { games_profile }                from '../../game/games_profile';
import {profile}      from '../../profile';





@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  //providers: [AuthService]

})
export class ProfileComponent implements OnInit {
  name:string     = '';
  loaded:boolean  = false;
  imageSrc:string = '';
  imageLoaded:boolean = false;
  model:any = [];
  //gameslist:games_profile[] = [];
  profiles:profile[] =[];
  checked_game:string[] = ["","",""];
  checked_platform:string[] = ["","","","",""];
  all_platform_list:string[] = [ 'PS', "PS3","PS4","Xbox","XB1"];



  gameslist:string[] = [];

  private userId_db;

  private currentGameID:string;

  img_src = "https://www.what-dog.net/Images/faces2/scroll0015.jpg";
  allowed_img_filesize:number = 1048576; //1Mb

  // checking current user to be logged in.
  constructor(private authService: AuthenticationService,
              private restService:RestService,
              private profilePicService:ProfilePicService,
              private router:Router,
              private gameService:GameService){
    this.getCurrentUserProfile();
    this.imageLoaded = false;
    if( this.authService.getCurrentUsername() == "guest"){
      this.model.bShowProfile = false;
    }else{
      this.model.bShowProfile = true;
    }

    // get use ID from backend-server
    this.userId_db = this.authService.getID();
    // get current user information every calling
    this.getProfiles();
  }


  ngOnInit() {
  }

  // get existed game lists
  onClicked(list,event){
    let cnt=0;
    console.log("event "+this.profiles.length);
    console.log("list "+list.gameName);
    console.log("event checked"+event.target.checked);
    console.log("event checked"+event.target.value);

    while( this.checked_game[cnt] != ""){
      cnt++;
    }
    console.log('cnt : '+cnt);
    console.log('game name : '+list.gameName);
    this.checked_game[cnt] = list.gameName;

  }

  // get current using user name
  getCurrentUserProfile(){
    this.name = this.authService.getCurrentUsername();
  }

  // change a picture for profile
  fileChangeEvent( fileInput:any ){
    let pattern = /image-*/;
    let newfile = fileInput.dataTransfer ? fileInput.dataTransfer.files[0] : fileInput.target.files[0];
    let reader;
    //checking new image file size
    if(newfile.size < this.allowed_img_filesize)
    {
      reader = new FileReader();
      this.model.imgsizeErr = false;
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsDataURL(newfile);

      if( !newfile.type.match(pattern)){
        alert('invalid format');
        return ;
      }
    }else{
      this.model.imgsizeErr = true;
    }
  }

  // get all existed profiles
  getProfiles() {
    //this.restService.setURL(  'http://localhost:8080/api/profiles' );
    this.restService.setURL(  'https://finalproject-kcho33.c9users.io:8080/api/profiles' );
    let dbgVal:profile[] =[];

    this.restService.GetRest()
                    .subscribe(
                      profiles  => {
                        this.profiles = profiles;
                        this.getValue(this.profiles);
                      });
  }

  // save current user ID of backend
  getValue(profiles:any){
    this.profiles = profiles;
    let current_user = localStorage.getItem('currentUser');
    console.log('subscirbe : ' + this.profiles);
    for( let profile of profiles){
      if( profile.username == current_user )
      {
        console.log('profile ID :' + profile._id );
        localStorage.setItem('UserID_DB', profile._id);
        return;
      }
    }
  }

  //  send changed profile to backend
  saveNewProfile(){
    console.log("new ID = " + this.model.PlayerID);
    console.log("MIC    = " + this.model.bUsingMIC);
    console.log("age   = " + this.model.age);
    console.log("time    = " + this.model.time);
    console.log('selected game = ' + this.model.selectedGame0);
    console.log('selected game = ' + this.model.selectedGame1);
    console.log('selected game = ' + this.model.selectedGame2);

    this.userId_db = localStorage.getItem('UserID_DB');

    console.log('current ID :' + this.userId_db);

    console.log('selected Game Name : ' + this.checked_game[0] );
    console.log('selected Game Name : ' + this.checked_game[1] );
    console.log('selected Game Name : ' + this.checked_game[2] );

    //let add_url = 'http://localhost:8080/api/profiles/'+this.userId_db;
    let add_url = 'https://finalproject-kcho33.c9users.io:8080/api/profiles/'+this.userId_db;
    this.restService.setURL( add_url );

    var headers = new Headers( {'Content-Type': 'application/x-www-form-urlencoded'});
    let options = new RequestOptions( {headers:headers});
    let data    = new URLSearchParams();
    data.append('age'  , this.model.age);
    data.append('playerID'  , this.model.PlayerID);

    this.restService.UpdateProfile(data, options)
                    .subscribe(
                    data => {
                    });


  }
  cancelEditProfile(){
    this.router.navigate(['/main']);
  }

  // check a platform that want to be changed
  onClickedPlatform(list,event){
    let cnt=0;
    //console.log("event "+this.profiles.length);
    console.log("list "+list);
    console.log("event checked"+event.target.checked);
    console.log("event checked"+event.target.value);

    while( this.checked_platform[cnt] != ""){
      cnt++;
    }
    //console.log('cnt : '+cnt);
    //console.log('game name : '+list);
    this.checked_platform[cnt] = list;

    //this.gamelistFilterByPlatform()
  }

  // list up current existed game list
  getCurrentGameList(){
    let gameitem:string[] = ["Ark","RainbowSix","Overwatch"];

    console.log('call getCurrentGameList()')
    this.gameslist = [];

    for( let item of this.checked_platform){
      console.log('checked platform :'+ item);

      if( item == ""){
        return;
      }else{
          for(let cnt=0; cnt<3; cnt++){
            let data    = new URLSearchParams();
            data.append('gameName'  , gameitem[cnt]);

            this.gameService.GetPlatformForGame(data)
                        .subscribe(
                          platforms => {
                            // avoiding to double-check related to list up game list
                            // just list up once every game list
                            for(let platform of platforms.platform)
                            {
                              console.log("return value :" + platform.platform_item);
                              console.log('item :'+item);
                              if( item == platform.platform_item)
                              {
                                  console.log('Bingo');

                                  //avoiding checking the game name more than one
                                  for(let item of this.gameslist)
                                  {
                                    if( item == gameitem[cnt] )
                                    {
                                      return;
                                    }

                                  }
                                  this.gameslist.push(gameitem[cnt]);
                                  return;
                              }

                            }
                          });
              }
              this.checked_platform = ["","","","",""];
      }
    }



  }





  private _handleReaderLoaded(e){
    var reader = e.target;
    this.img_src = reader.result;
    this.loaded = true;
  }

  upload(){
    this.imageLoaded = true;
  }

}
