import { Component, OnInit } from '@angular/core';
import { AuthenticationService, AlertService,RestService} from '../../servicesForAccount';
import { Router, ActivatedRoute } from '@angular/router';
import { Headers,RequestOptions,URLSearchParams }    from '@angular/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [AuthenticationService,AlertService,RestService]

})
export class LoginComponent implements OnInit {
  model: any = {};
  loading = false;
  returnUrl: String;
  username = '';



  constructor(private restService: RestService,
              private authService: AuthenticationService,
              private alertService:AlertService,
              private router: Router,
              private activatedRoute: ActivatedRoute){
                console.log('call login component');

             }

  
  ngOnInit() {
    // log out every initalization
    this.authService.logout();
    // save return url
    this.returnUrl = this.activatedRoute.snapshot.queryParams['returnUrl'] || '/';
    console.log('returl url : '+ this.returnUrl);
  }

  // send username and password to login to backend-server 
  login(){
    console.log('The Class login function....')
    var headers = new Headers( {'Content-Type': 'application/x-www-form-urlencoded'});
    let options = new RequestOptions( {headers:headers});
    /*let data = {
      'username': this.model.username, 'password':this.model.password
    };*/
    this.restService.setURL('https://finalproject-kcho33.c9users.io:8080/api/login' );
    //this.restService.setURL('https://finalproject-kcho33.c9users.io:8080/api/login' ); 
    let data    = new URLSearchParams();
    data.append('username'  , this.model.username);
    data.append('password'  , this.model.password);

    // call service to send the back-end server
    this.restService.LoginPost(data, options)
      .subscribe(
        data => {
          this.router.navigate( [this.returnUrl] );
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });

  }

  // call when to login with google account
  googleLogin(){
    this.authService.auth('google');
  }


}
