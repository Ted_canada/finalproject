import { Component, OnInit } from '@angular/core';

import { GamesRoutingModule } from '../game/game-routing-module';
import { AuthenticationService } from '../services/authentication.service';
import { Router } from '@angular/router';

import { RestService } from '../services/rest.service'

import {profile}      from '../profile';





@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {

  model: any = {};
  member_count: number;
  profile_db_url = 'http://localhost:8080/api/profiles';
  //profile_db_url = 'https://finalproject-kcho33.c9users.io:8080/api/profiles';
  profiles:profile[] =[];
  errorMessage: string;



  //constructor( login: LoginComponent ) {  }
  constructor( private authService: AuthenticationService,
               private router: Router,
               private restService:RestService) {
                 console.log('call main page');

                 this.model.currentUser  = this.authService.getCurrentUsername();
                 console.log('current user :' + this.model.currentUser);
                 this.model.userStatus = this.getUserStatus(this.model.currentUser);
                 //this.member_count = 5;
                 this.countTotalUser();
  }

  ngOnInit() {}

  logout(){
    this.authService.logout();
    this.router.navigate(['/main']);

  }

  // get current total user
  countTotalUser():number{
    let total_user_number = 0;


    this.restService.setURL(this.profile_db_url);
    this.restService.GetRest()
                      .subscribe(
                        profiles  => {
                          this.profiles = profiles;
                          this.model.membernumber = profiles.length;
                          },
                        error     => this.errorMessage  = <any>error );
    console.log('value : '+ this.profiles.length);


    return total_user_number;
  }



  // check whether someone log-in or not
  private getUserStatus( currentUser:string ):boolean{
    if( currentUser == "guest" ){
      return false;
    }
    return true;
  }




}
