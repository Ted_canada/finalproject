import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Headers,RequestOptions, URLSearchParams }    from '@angular/http';

import { Gamelist, GameService }        from '../game.service';
import { AuthenticationService } from '../../servicesForAccount';
import { games_profile }                from '../games_profile';
import { lfg_profile }                  from '../lfg_profile';


@Component({
  selector: 'app-game-detail',
  templateUrl: './game-detail.component.html',
  styleUrls: ['./game-detail.component.css'],
  providers: [GameService]

})
export class GameDetailComponent implements OnInit {
  model: any = {};
  game: Gamelist;
  currentGame:  games_profile;
  lfgProfile:lfg_profile[] = [];
  selectedGame ="";
  selectedPlatform ="";
  selectedPlay ="";
  filter_conditions: any[] = [{ playtime: '' }, { game: '' }, { platform: '' }];

  users: any[] = [{ platform: 'XBOX' }, { platform: 'PS' }, { platform: 'PS4' }];
  games: any[] = [{ game: '' },];
  platformFilter: any = { platform: '' };



  constructor( private router:Router,
               private route:ActivatedRoute,
               private authService:AuthenticationService,
               private gameService:GameService) {


              //save the selected game list

              this.model.game_name        = localStorage.getItem('selected_game_name');
              this.model.game_refImgPath  = localStorage.getItem('selected_game_refImgPath');
              this.model.game_title       = localStorage.getItem('selected_game_title');
              //this.model.game_bgimg       = localStorage.getItem('selected_game_bgimg');
              this.model.game_bgimg       = 'https://cdn.lfg.pub/images/activities/ark-survival-evolved/banner.jpg';


              // checking whether current user is logged in.
              if( this.authService.getCurrentUsername() != "guest" ){
                  this.model.loginStatus = true;
              }else{
                this.model.loginStatus = false;
              }


              // get information for LFG
              this.getLFG();
             }

  ngOnInit() {
    console.log('call game detail page');
    /*this.route.params
        .switchMap( (params:Params) => this.gameService.getGame(+params['id']))
        .subscribe( (game:Gamelist) => this.game = game); */
  }


  //get all LFG information from backend server
  getLFG(){
    console.log('Call getLFG()');
    let data    = new URLSearchParams();
    data.append('gameName'  , this.model.game_name  );

    this.gameService.GetLfgForGame(data)
                .subscribe(
                  lfglists =>{ this.lfgProfile = lfglists
                    for(let lfglist of lfglists){

                      console.log('lfg list : '+lfglist.LFGTeamName );
                      console.log('lfg list : '+lfglist.LFGCreater );
                      console.log('lfg list : '+lfglist.playStyle );
                      console.log('lfg list : '+lfglist.playtime );
                      console.log('lfg list : '+lfglist.Description );
                      console.log('lfg list : '+lfglist.gameName );
                      console.log('lfg list : '+lfglist.platformName );


                    }

                  }

                  );

  }

  FilterByGame(item){
    //console.log(item.value);
    console.log("select team name : "+ this.selectedGame);
    this.filter_conditions[0].game = this.selectedGame;
    console.log('value : ' + this.filter_conditions[0].game);
  }

  FilterByPlatform(item){
    //console.log(item.value);
    console.log("select team name : "+ this.selectedPlatform);
    this.platformFilter.platform = this.selectedPlatform;
    console.log('value : ' + this.platformFilter.platform);
  }

  FilterByPlaytime(item){
    //console.log(item.value);
    console.log("select team name : "+ this.selectedPlay);
    this.filter_conditions[0].playtime = this.selectedPlay;
    console.log('value : ' + this.filter_conditions[0].playtime);
    //this.filter_conditions.pu
  }


  RemoveLFG()
  {
    console.log("Remove LFG");
  }

}
