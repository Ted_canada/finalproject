export interface games_profile {
    game_id         : number;
    game_name       : string;
    gameTitle      : string;
    gameRefImgPath : string;
    gameBgImg      : string;
}
