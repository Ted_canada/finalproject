import { Component, OnInit , Input} from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { URLSearchParams }    from '@angular/http';

//import { FormsModule } from '@angular/forms';

import 'rxjs/add/operator/switchMap';
import { Observable }   from 'rxjs/Observable';

import { GamesRoutingModule }           from '../game-routing-module';
import { Gamelist, GameService }        from '../game.service';

import { games_profile}                from '../games_profile';




@Component({
  selector: 'app-game-main-page',
  templateUrl: './game-main-page.component.html',
  styleUrls: ['./game-main-page.component.css'],
  providers: [GameService]
})
export class GameMainPageComponent implements OnInit {
  games: Observable<Gamelist[]>;
  private selectedId: number;
  gamelists : games_profile[] = [];
  model: any = {};
  private current_game_num:number = 3;


  //item:Gamelist;
  constructor(private router:Router,
              private route:ActivatedRoute,
              private gameService:GameService ) {

                console.log('GameUploadDone status: '+localStorage.getItem("GameUploadDone"));
                this.gameService.GetGameInfoRest()
                    .subscribe(
                      data=> {
                        console.log('data :'+data.length);
                        //if( data.length === 0)
                        //{
                          console.log("DB have nothing")
                          this.updateGamelist(data.length);

                        //}
                      }
                    );
              }


  ngOnInit() {
    console.log('call game main page');
    localStorage.setItem("GameUploadDone", "false");
    //localStorage.setItem("PlatformUploadDone", "false");
  }

  // initalize all of game lists
  private updateGamelist(data_length:number){
    let gameInfo    = new URLSearchParams();
    console.log("Game list Uploading");

    this.gamelists[0] = { game_id         : 0,
                          game_name       : "Ark",
                          gameRefImgPath : "https://cdn.lfg.pub/images/activities/ark-survival-evolved/icon.jpg",
                          gameBgImg       :"",
                          //username        :"",
                          //userGameID      : "",
                          gameTitle      : "Ark: Survival Evolved - Survive, together!"

                        };
    this.gamelists[1] = { game_id         : 1,
                          game_name       :"RainbowSix",
                          gameRefImgPath :"https://cdn.lfg.pub/images/activities/rainbow-six-siege/icon.jpg",
                          gameBgImg       :"",
                          //username        :[],
                          //userGameID      : [],
                          gameTitle      :"Rainbow Six : Siege - LFG, LFM &amp; LFT"

                        };
    this.gamelists[2] = { game_id         : 2,
                          game_name       :"Overwatch",
                          gameRefImgPath :"https://cdn.lfg.pub/images/activities/overwatch/icon.jpg",
                          gameBgImg       :"",
                          //username        :[],
                          //userGameID      : [],
                          gameTitle      :"Overwatch - LFG, LFM &amp; LFT. Team up!"

                        };

    if( data_length == 0){
      for(let gamelist of this.gamelists){
        gameInfo    = new URLSearchParams();
        gameInfo.append("gameID", String(gamelist.game_id));
        gameInfo.append("gameName", gamelist.game_name);
        gameInfo.append("gameRefImgPath", gamelist.gameRefImgPath);
        gameInfo.append("gameBgImg", gamelist.gameBgImg);
        gameInfo.append("gameTitle", gamelist.gameTitle);

        this.gameService.UploadGames(gameInfo)
            .subscribe(
              data=>{ },
              error=>{}
            );
      }
    }

    // allow to create game list just once.
    if(localStorage.getItem("PlatformUploadDone") != "true"){
      this.updatePlatformInfo();
    }

    localStorage.setItem("GameUploadDone", "true");
  }


  // Update platform information
  private updatePlatformInfo(){
    let platformInfo; //   = new URLSearchParams();
    let platform_list1:string[] =  [ 'PS', "PS3","PS4","Xbox","XB1"];
    let platform_list2:string[] =  [ 'PS',"PS4","Xbox"];
    let platform_list3:string[] =  [ 'PS'];
    console.log("platform list uploading")

    console.log('Ark');
    for( let platform of platform_list1 ){
      console.log('item1 :'+ platform);
      platformInfo    = new URLSearchParams();
      platformInfo.append("gameName", "Ark" );
      platformInfo.append("platform_item", platform );

      this.gameService.UploadPlatform(platformInfo)
          .subscribe(
            data=>{ },
            error=>{}
          );
    }
    console.log('RainbowSix');
    for( let platform of platform_list2 ){
      console.log('item2 :'+ platform);
      platformInfo    = new URLSearchParams();
      platformInfo.append("gameName", "RainbowSix" );
      platformInfo.append("platform_item", platform );

      this.gameService.UploadPlatform(platformInfo)
          .subscribe(
            data=>{ },
            error=>{}
          );
    }
    console.log('Overwatch');
    for( let platform of platform_list3 ){
      console.log('item3 :'+ platform);
      platformInfo    = new URLSearchParams();
      platformInfo.append("gameName", "Overwatch" );
      platformInfo.append("platform_item", platform );

      this.gameService.UploadPlatform(platformInfo)
          .subscribe(
            data=>{ },
            error=>{}
          );
    }

    localStorage.setItem("PlatformUploadDone", "true");
  }
  isSelected( game:Gamelist ) { return game.game_item === this.selectedId; }

  OpenGameDetail( game_id:number ){
    console.log('OpenGameDetail :'+game_id);



    //if( localStorage.getItem("GameUploadDone") != "true" ){
    this.gameService.setCurrentGame(this.gamelists[game_id]);
    //}
      /*this.games = this.route.params
              .switchMap( (params:Params) => {
                this.selectedId = 2;
                console.log('this.selectedI :'+this.selectedId);
                this.selectedId = +params['id'];
                return this.gameService.getGames();
              });*/
      this.router.navigate(['/game-detail-page/',game_id]);

  }
}
