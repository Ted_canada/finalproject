import { Component, OnInit } from '@angular/core';
import { Router }             from '@angular/router';
import { Headers,RequestOptions, URLSearchParams }    from '@angular/http';

import { GameService }        from '../game.service';
import { AuthenticationService } from '../../servicesForAccount';

import { games_profile }                from '../games_profile';


@Component({
  selector: 'app-game-lfg',
  templateUrl: './game-lfg.component.html',
  styleUrls: ['./game-lfg.component.css']
})
export class GameLfgComponent implements OnInit {
  name:string     = '';
  model:any = [];
  gameslist:games_profile[] = [];
  checked_game:string = "";
  checked_playStyle:string = "";

  checked_platform:string = "";
  PlayerID = "";
  play_styles:string[] = ["Any","Casual","Serious","Zealous"];
  gameitems:string[] =  [ 'Ark', "RainbowSix","Overwatch"];
  platforms:string[] =  [ 'PS', "PS3","PS4","Xbox","XB1"];




  constructor(  private gameService:GameService,
                private authService: AuthenticationService,
                private router:Router){

    // initalize current login status
    this.model.login_status =true;
    // list up current all of games
    this.getCurrentGameList();

  // get current user profile logging in
    this.getCurrentUserProfile();
  }

  ngOnInit() {
  }


  // list up current all of games

  getCurrentGameList(){
    console.log('game list up');
    this.gameService.GetGameInfoRest()
                .subscribe(
                  games  => this.gameslist = games
                );
  }


  // get current user profile logging in
  getCurrentUserProfile(){
    this.name = this.authService.getCurrentUsername();
  }


  // select one game 
  onClickedGame(list,event){
    let cnt=0;
    //console.log("event "+this.profiles.length);
    console.log("list "+list);
    console.log("event checked"+event.target.checked);
    console.log("event checked"+event.target.value);

    this.checked_game  = list;

  }
  // select game platform
  onClickedplatform(list,event){
    let cnt=0;
    //console.log("event "+this.profiles.length);
    console.log("list "+list);
    console.log("event checked"+event.target.checked);
    console.log("event checked"+event.target.value);

    this.checked_platform = list;

  }
  // select game style
  onClickedPlayStyle(list,event){
    let cnt=0;
    //console.log("event "+this.profiles.length);
    console.log("list "+list);
    console.log("event checked"+event.target.checked);
    console.log("event checked"+event.target.value);


    this.checked_playStyle = list;

  }


  // request to create new LFG 
  RequestLFG(){
    console.log('selected Creater : ' + this.name );
    console.log('selected Game Name : ' + this.checked_game );
    console.log('selected play style : ' + this.checked_playStyle );
    console.log('selected time : ' + this.model.time );
    console.log('selected description : ' + this.model.description );
    console.log('selected teamName : ' + this.model.teamName );

    var headers = new Headers( {'Content-Type': 'application/x-www-form-urlencoded'});
    let options = new RequestOptions( {headers:headers});
    let lfg_info    = new URLSearchParams();
    lfg_info.append('lfgCreater'  , this.name );
    lfg_info.append('gameName'  , this.checked_game);
    lfg_info.append('playStyle'  , this.checked_playStyle);
    lfg_info.append('playertime'  , this.model.time);
    lfg_info.append('describe'  , this.model.description);
    lfg_info.append('LFGTeamName'  , this.model.teamName);
    lfg_info.append('gameName'  , this.model.gameName);
    lfg_info.append('platformName'  , this.checked_platform);

    this.gameService.UploadLfg(lfg_info)
        .subscribe(
          data=>{ },
          error=>{}
        );
  }

  canecelEditProfile(){
    this.router.navigate(['/game-main-Page']);

  }


}
