import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameLfgComponent } from './game-lfg.component';

describe('GameLfgComponent', () => {
  let component: GameLfgComponent;
  let fixture: ComponentFixture<GameLfgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameLfgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameLfgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
