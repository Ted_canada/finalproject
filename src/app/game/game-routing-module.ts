import { Routes, RouterModule }   from '@angular/router';
import { NgModule }               from '@angular/core';

import { GameMainPageComponent }  from './game-main-page/game-main-page.component';
import { GameHomePageComponent }  from './game-home-page/game-home-page.component';
import { GameDetailComponent }    from './game-detail/game-detail.component';
import { GameLfgComponent }       from './game-lfg/game-lfg.component';

const gamesRoutes: Routes = [
    /*{
      path: '',
      redirectTo:'/gameMainPage',
      pathMatch:'full'
    },
    {
      path: 'gameMainPage',
      component: GameMainPageComponent
    },
    {
      path:'game-detail-page/:id',
      component: GameDetailComponent
    }*/
    {
      path:'',
      component:GameMainPageComponent
    },
    {
      path:'game-detail-page/:id',
      component:GameDetailComponent
    },
    {
      path:'game-lfg',
      component:GameLfgComponent
    }


];

@NgModule({
  imports:[
    RouterModule.forChild(gamesRoutes)
  ],
  exports:[
    RouterModule
  ]

})

export class GamesRoutingModule {}
