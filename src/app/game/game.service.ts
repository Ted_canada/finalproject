import { Injectable } from '@angular/core';
import { Http, Response }           from '@angular/http';
import { Headers,RequestOptions, URLSearchParams }    from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';


import { games_profile }                from './games_profile';
//import {gameInfo}      from '../gameInfo';


export class Gamelist{
  constructor( public id:number, public game_item){}
}

let GAMES = [
  new Gamelist( 1, 'Ark')
];

let gamePromise = Promise.resolve(GAMES )

@Injectable()
export class GameService {

  /*gameDB_url = "http://localhost:8080/api/games";
  gameDB_LFG_url = "http://localhost:8080/api/games/lfg";
  gameDB_LFGDetail_url = "http://localhost:8080/api/games/lfg/detail";
  gameDB_platform_url= "http://localhost:8080/api/games/platform";
  gameDB_Getplatform_url= "http://localhost:8080/api/games/getplatform";*/
  
  // rouing path for each services
  gameDB_url = "https://finalproject-kcho33.c9users.io:8080/api/games";
  gameDB_LFG_url = "https://finalproject-kcho33.c9users.io:8080/api/games/lfg";
  gameDB_LFGDetail_url = "https://finalproject-kcho33.c9users.io:8080/api/games/lfg/detail";
  gameDB_platform_url= "https://finalproject-kcho33.c9users.io:8080/api/games/platform";
  gameDB_Getplatform_url= "https://finalproject-kcho33.c9users.io:8080/api/games/getplatform";
  gamelists : games_profile[] = [];


  getGames() {
    console.log('call getGames() : '+ gamePromise);
    return gamePromise;
  };

  constructor(private http: Http) {
    console.log('call game service()');
  }

  // get game page ID to be selected
  getGame( id: number | string){
    console.log('passed id : '+id);
    return gamePromise.
      then(games => games.find( game => game.id === +id));
  }


  // setting information related to current selected game
  setCurrentGame( cur_gamelist:games_profile ){
    localStorage.setItem('selected_game_name',        cur_gamelist.game_name);
    localStorage.setItem('selected_game_refImgPath',  cur_gamelist.gameRefImgPath);
    localStorage.setItem('selected_game_title',       cur_gamelist.gameTitle);
  }


  // Get game list from DB
  GetGameInfoRest(): Observable<games_profile[]>{
    console.log('call GetGameInfoRest()');
    return this.http.get(this.gameDB_url)
                      .map( (res:Response)=> {
                        return res.json();
                      })
                      .catch(this.handleError);
  }
  
  // Get platform from DB

  GetPlatformForGame(data:any): Observable<any>{
    console.log('call GetPlatformForGame()');
    var headers = new Headers( {'Content-Type': 'application/x-www-form-urlencoded'});
    let options = new RequestOptions( {headers:headers});
    return this.http.post(this.gameDB_Getplatform_url, data, options )
                      .map( (res:Response)=> {
                        return res.json();
                      })
                      .catch(this.handleError);
  }
  
  
  // Get LFG from DB
  GetLfgForGame(data:any): Observable<any>{
    console.log('call GetLfgForGame()');
    var headers = new Headers( {'Content-Type': 'application/x-www-form-urlencoded'});
    let options = new RequestOptions( {headers:headers});
    return this.http.post(this.gameDB_LFGDetail_url, data, options )
                      .map( (res:Response)=> {
                        return res.json().lfgData;
                      })
                      .catch(this.handleError);
  }

  // upload new game
  UploadGames( data: any ):Observable<games_profile[]>{
    var headers = new Headers( {'Content-Type': 'application/x-www-form-urlencoded'});
    let options = new RequestOptions( {headers:headers});

    return this.http.post(this.gameDB_url, data, options )
                    .map((res:Response)=> {
                    })
                    .catch(this.handleError);
  }

  // upload new platform
  UploadPlatform( data: any ):Observable<games_profile[]>{
    var headers = new Headers( {'Content-Type': 'application/x-www-form-urlencoded'});
    let options = new RequestOptions( {headers:headers});

    return this.http.post(this.gameDB_platform_url, data, options )
                    .map((res:Response)=> {
                    })
                    .catch(this.handleError);
  }

  // upload new LFG
  UploadLfg( data: any ):Observable<any[]>{
    var headers = new Headers( {'Content-Type': 'application/x-www-form-urlencoded'});
    let options = new RequestOptions( {headers:headers});

    return this.http.post(this.gameDB_LFG_url, data, options )
                    .map((res:Response)=> {
                    })
                    .catch(this.handleError);
  }

  private handleError(error:Response | any ){
      let errMsg:string;
      if(error instanceof Response){
        const body = error.json();
        const err = body.error || JSON.stringify(body);
        errMsg = '${err}';
      }else{
        errMsg = error.message ? error.message : error.toString();
      }
      //console.log(errMsg);
      return Observable.throw(errMsg);
    }

}
