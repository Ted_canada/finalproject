import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { GameMainPageComponent}  from './game-main-page/game-main-page.component';
import { GamesRoutingModule }    from './game-routing-module';
import { GameDetailComponent }   from './game-detail/game-detail.component';

import { GameService }        from './game.service';
import { GameLfgComponent } from './game-lfg/game-lfg.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    GamesRoutingModule
  ],
  declarations: [
    GameMainPageComponent,
    GameDetailComponent,
    GameLfgComponent
  ],
  providers: [GameService]
})
export class GamesModule { }
