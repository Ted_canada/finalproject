import { Injectable } from '@angular/core';
import { Http, Headers, Response,URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { RequestOptions }    from '@angular/http';
import 'rxjs/add/operator/map';

import { InterceptorService } from 'ng2-interceptors';
import {  Router,Route, NavigationStart,
          Event as NavigationEvent,
          NavigationCancel,
          RoutesRecognized,
          CanActivate,CanActivateChild,CanLoad,
          ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';


@Injectable()
export class AuthenticationService implements CanActivate, CanActivateChild, CanLoad{
  private baseURL = 'https://finalproject-kcho33.c9users.io:8081';

  private configObj = {"authEndpoint":"","clientId":"","redirectURI":""};
  private code:string;
  private cachedURL:string;

  private loading: boolean;

  private authConfig = {
   
   "google":{
     "authEndpoint": this.baseURL+"/api/auth/google",
     //"clientId":"810514142409-2aova8e6go17p0g02110b2ilcip2v2m5.apps.googleusercontent.com",
     "clientId":"810514142409-t1183flv853v403iedp2og0mrgbb04j2.apps.googleusercontent.com",
     "redirectURI" : this.baseURL+"/admin"
   }
 };


  constructor(  private http: Http,
                private _http: InterceptorService,
                private router:Router ) {
    console.log('call constructor for auth');
    let config = localStorage.getItem("authConfig");
    if(config != ""){
      this.configObj = JSON.parse(config);
    }
    console.log('passing check authconfig');
}


  // log-in with the thirdparty account 
  threepartylogin(code:any,clientId:any,redirectURI:any,authEndpoint:any):Observable<any>{
      console.log('logging in....');

      console.log('call login function');
      var headers = new Headers( {'Content-Type': 'application/x-www-form-urlencoded'});
      let options = new RequestOptions( {headers:headers});

      //var body = {"code" : code,"clientId" : clientId,"redirectUri":redirectURI}
      let body    = new URLSearchParams();
      body.append('code'  , code);
      body.append('clientId'  , clientId);
      body.append('redirectUri'  , redirectURI);

      console.log("code : "+ code);
      console.log("clientId : " + clientId);
      console.log("redirectUri : " +redirectURI);

      let url = "https://finalproject-kcho33.c9users.io:8080/api/auth/google";
      //let url = "https://finalproject-kcho33.c9users.io:8080/api/auth/google";
      return this.http.post(url, body, options)
          .map((response: Response) => {
            localStorage.setItem('isLoggedIn', "true");
            localStorage.setItem('token', response.json().token);
            localStorage.setItem('currentUser', response.json().user);
            this.router.navigate(['/login']);
            return response.json();
          });
  }

  // get current user name
  getCurrentUsername():any{
    let currentUser;

    if( localStorage.getItem('currentUser') == null){
      return "guest";
    }else{
      return localStorage.getItem('currentUser');
    }
  }

  // get User ID of DB
  getID():any{
    return localStorage.getItem('UserID_DB');
  }

 canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    let url: string = state.url;
    console.log('call canActivate(), url :'+ url );

    // split url information for get meaningful information
    let params = new URLSearchParams(url.split('?')[1]);
    this.code = params.get('code');

    return this.verifyLogin(url);
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    console.log('call canActivateChild(), route : '+ route + ' state : '+ state );
    return this.canActivate(route, state);
  }

  canLoad(route: Route): boolean {
    let url = `/${route.path}`;

    return this.verifyLogin(url);
  }

  // checking whether login or not
 verifyLogin(url) : boolean{

     //console.log('cachedURL: ' + url.split('?')[0]);
     localStorage.setItem('cachedurl',url.split('?')[0]);
     this.cachedURL = localStorage.getItem('cachedurl');
    if(!this.isLoggedIn() && this.code == null){
      //console.log('No Logging in, but code alive');
      //console.log('this code : '+ this.code);
      //console.log('this.isLoggedIn() : '+ this.isLoggedIn());
      this.router.navigate(['/main']);
      return true;
    }
    else if(this.isLoggedIn()){
      //console.log('still Logging in....')
      //this.router.navigate(['/login']);
      return true;
    }
    else if(!this.isLoggedIn() /* && this.code != null */){

      /*console.log("This.code : "+ this.code);
      console.log("This.clientId : "+ this.configObj.clientId);
      console.log("This.authEndpoint : "+ this.configObj.redirectURI);
      console.log("This.code : "+ this.configObj.authEndpoint);*/

      this.loading = true;

      // attempt to log-in
      this.threepartylogin(this.code, this.configObj.clientId, this.configObj.redirectURI, this.configObj.authEndpoint)
      .subscribe((data:any) => {
          this.loading = false;
          this.router.navigate([this.cachedURL]);
              return true;
          });
          return true;
  }
}

  // log out
  logout(): void {
    console.log('Log out.....');
    this.code = null;
    // save status related to logout
    localStorage.setItem('isLoggedIn',"false");
    localStorage.removeItem('token');
    localStorage.removeItem('currentUser');
    localStorage.removeItem('UserID_DB');
    //this.router.navigate(['/login']);
    //this.router.navigate(['/main']);
  }


  GetAuthToken( item:string ){
    return this.jwt(item);
  }

  private jwt( item:string) {
        // create authorization header with jwt token
        let currentItem = JSON.parse(localStorage.getItem(item));

        if (currentItem && currentItem.token) {
            let headers = new Headers({ 'Authorization': 'Bearer ' + currentItem.token });
            return new RequestOptions({ headers: headers });
        }
  }


  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }

  /*verifyLogin(url):boolean{
       localStorage.setItem('cachedurl',url.split('?')[0]);
       this.cachedURL = localStorage.getItem('cachedurl');
      if(!this.isLoggedIn() && this.code == null){
        this.router.navigate(['/login']);
        return false;
      }
      else if(this.isLoggedIn()){
        return true;
      }
      else if(!this.isLoggedIn()  && this.code != null){
        this.loading = true;
        this.login(this.code,this.configObj.clientId,this.configObj.redirectURI,this.configObj.authEndpoint)
        .then((data:any) => {
            this.loading = false;
            this.router.navigate([this.cachedURL]);
                return true;
            });
      }
    }*/

  private isLoggedIn(): boolean{
    let status = false;
    if( localStorage.getItem('isLoggedIn') == "true"){
      status = true;
    }
    else{
      status = false;
    }
    return status;
  }

  ///// for functions of oAuth ///////////////

  public auth(provider:string):void{
    console.log('call auth for 3rd party');

    console.log('this.isLoggedIn() : '+ this.isLoggedIn() );
    /*
    if(provider == "linkedin" && !this.isLoggedIn()){
      localStorage.setItem("authConfig",JSON.stringify(this.authConfig.linkedin));
      window.location.href = 'https://www.linkedin.com/oauth/v2/authorization?client_id='+this.authConfig.linkedin.clientId+'&redirect_uri='+this.authConfig.linkedin.redirectURI+'&response_type=code';
  }
   if(provider == "facebook" && !this.isLoggedIn()){
      localStorage.setItem("authConfig",JSON.stringify(this.authConfig.facebook));
       window.location.href = 'https://www.facebook.com/v2.8/dialog/oauth?client_id='+this.authConfig.facebook.clientId+'&redirect_uri='+this.authConfig.facebook.redirectURI+'&scope=email';
  } */
   if(provider == "google" && !this.isLoggedIn()){
      localStorage.setItem("authConfig",JSON.stringify(this.authConfig.google));
       window.location.href = 'https://accounts.google.com/o/oauth2/v2/auth?response_type=code&client_id='+this.authConfig.google.clientId+'&redirect_uri='+this.authConfig.google.redirectURI+'&scope=email%20profile';
  }
    else{
        console.log('this.router.navigate([this.cachedURL]);' + this.cachedURL);

        this.router.navigate([this.cachedURL]);
    }
  }


}
