import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, URLSearchParams,Response }     from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class EmailVerifyService {

  //private current_url = `https://localhost:8080/api/sendmail`;


  //private server_url = `http://localhost:8090`;
  private current_url = `https://finalproject-kcho33.c9users.io:8080/api/sendmail`;
  //private server_url = `https://finalproject-kcho33.c9users.io:8090`;


  constructor( private http: Http ) { }
 
  //  send to verify email
  verifyEmail( email_addr:string ):Observable<any>{
    console.log('call a function for Verify Email');

    var headers = new Headers( {'Content-Type': 'application/x-www-form-urlencoded'});
    let options = new RequestOptions( {headers:headers});

    let data    = new URLSearchParams();
    data.append('emailaddr', email_addr );

    return this.http.post(this.current_url, data, options)
        .map((response: Response) => {
          //localStorage.setItem('token', response.json().status);
          return response.json().status;
        });
  }
  private handleError(error:Response | any ){
      console.log('Posst error');
      let errMsg:string;
      if(error instanceof Response){
        const body = error.json();
        const err = body.error || JSON.stringify(body);
        errMsg = '${err}';
      }else{
        errMsg = error.message ? error.message : error.toString();
      }
      //console.log(errMsg);
      return errMsg;
    }

}
