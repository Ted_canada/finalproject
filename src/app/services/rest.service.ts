import { Injectable }               from '@angular/core';
import { Http, Response }           from '@angular/http';
import { Headers,RequestOptions }    from '@angular/http';


import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
//import 'rxjs/add/operator/toPromise';

import {profile}      from '../profile';



@Injectable()
export class RestService {

  private client_url;
  profiles: profile[]= [];
  result= Object;

  constructor(private http: Http) { }


  setURL( url:string ){
    this.client_url = url;
  }

  // Get request
  GetRest(): Observable<profile[]>{
    return this.http.get(this.client_url)
                      .map( this.extractData)
                      .catch(this.handleError);
  }

  // Post request for creating something
  CreateRest( data: any, options ):Observable<profile>{
    return this.http.post(this.client_url, data, options )
                    .map(this.extractData)
                    .catch(this.handleError);
  }

  // Post request for loging
  LoginPost( data: any, options ):Observable<profile>{
    return this.http.post(this.client_url, data, options )
                    .map((res:Response) =>{
                      localStorage.setItem('isLoggedIn', "true");
                      localStorage.setItem('token', res.json().token);
                      localStorage.setItem('currentUser', res.json().user);

                    })
                    .catch(this.handleError);
  }
  
  
  // Post request for updating profile
  UpdateProfile( data:any, options):Observable<profile>{
    return this.http.post(this.client_url, data, options )
                    .map((res:Response) =>{
                    })
                    .catch(this.handleError);
  }



  private extractData(res: Response){
      let body=res.json();
      //console.log('[extractData] DB_ID : '+ body.body.username);

      return body;
  }


  private handleError(error:Response | any ){
      let errMsg:string;
      if(error instanceof Response){
        const body = error.json();
        const err = body.error || JSON.stringify(body);
        errMsg = '${err}';
      }else{
        errMsg = error.message ? error.message : error.toString();
      }
      //console.log(errMsg);
      return Observable.throw(errMsg);
    }


}
