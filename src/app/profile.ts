export class profile {
  constructor(
    public id         : number,
    public username   : string,
    public firstname  : string,
    public lastname   : string,
    public gended     : string,
    public email      : string){}
}
