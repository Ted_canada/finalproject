var mongoose    =   require('mongoose');
var Schema      =   mongoose.Schema;

var GamerSchema = new Schema({
  //username: String,
  userPlayerID:String
});

var PlatformSchema = new Schema({
  platform_item:String
  /*,
  usingMIC:{
    type:Boolean
  }*/
});

/*var LfgSchema = new Schema({
  LFGCreater:{
    type:String
  },
  playStyle:{
    type:String
  },
  playtime:{
    type:String
  },
  Description:{
    type:String
  }
});*/

var LfgSchema = new Schema({
  LFGTeamName: String,
  LFGCreater: String,
  playStyle:String,
  playtime:String,
  Description:String,
  gameName:String,
  platformName:String
});


var gamesSchema  =   new Schema({
    gameID:{
      type: String
    },
    gameName:{
      type: String
    },
    gameTitle:{
        type: String
    },
    gameRefImgPath:{
        type: String
    },
    gameBgImg:{
        type: String
    },
    PlayTime:{
        type: String
    },
    GameUsers :[GamerSchema],
    Platforms :[PlatformSchema],
    LFG       :[LfgSchema]
    /*LFG:{
      LFGTeamName: String,
      LFGCreater: String,
      playStyle:String,
      playtime:String,
      Description:String
    }*/
});



module.exports =  mongoose.model('PlatformDB', PlatformSchema);
module.exports =  mongoose.model('GamesUserDB', GamerSchema);
module.exports =  mongoose.model('LfgDB', LfgSchema);
module.exports =  mongoose.model('GamesDB', gamesSchema);
