var mongoose    =   require('mongoose');
var Schema      =   mongoose.Schema;

var profileSchema  =   new Schema({
    username:{
        type: String
    },
    playerID:{
        type: String
    },
    password:{
        type: String
    },
    firstname:{
        type: String
    },
    lastname:{
        type: String
    },
    age:{
        type: String
    },
    gended:{
        type: String
    },
    email:{
        type: String
    },
    picture:{
        data: Buffer,
        type: String
    }

});

module.exports =  mongoose.model('Profile', profileSchema);
