var mongoose    =   require('mongoose');
var crypto      =   require('crypto');
var jwt         =   require('jsonwebtoken');
var Schema      =   mongoose.Schema;

var userSchema  =   new Schema({
  email: {
    type:String
  },
  username:{
    type:String
  },
  hash:String,
  salt:String
});

userSchema.methods.setPassword = function(password){
  // the maximam length of password is 256

  this.salt = crypto.randomBytes(256).toString('hex');
  this.hash = crypto.pbkdf2Sync(password, this.salt, 100000, 512,'sha512').toString('hex');

  let hash2 = crypto.pbkdf2Sync(password, this.salt, 100000, 512,'sha512').toString('hex');
  /*console.log('username :'+ this.username);
  console.log('password :'+ password+"\n");

  console.log('hash : '+ this.hash+"\n");
  console.log('setting salt :'+this.salt+"\n");

  console.log('hash : '+ hash2+"\n\n");*/

};

userSchema.methods.validPassword =  function(password){
  /*console.log('username2 :'+ this.username+"\n");
  console.log('password2 :'+ password+"\n");
  console.log('salt2 :'+this.salt+"\n");*/
  var hash = crypto.pbkdf2Sync(password, this.salt, 100000, 512,'sha512').toString('hex');

  //console.log('hash2 :'+ hash+"\n");

  //console.log('this.hash :'+ this.hash+"\n");

  /*console.log('password to valify :'+ password)
  console.log('this.hash : '+this.hash);
  console.log('hash : '+hash);*/
  return this.hash === hash;
};

userSchema.methods.generateJwt =  function(){
  var expiry = new Date();
  expiry.setDate(expiry.getDate() + 7);


  return jwt.sign({
    _id:        this._id,
    email:      this.email,
    username:   this.username,
    exp: parseInt(expiry.getTime()/1000),
  }, "MY_SECRET");
};


module.exports =  mongoose.model('Users', userSchema);
