var mongoose  = require('mongoose');
var passport  = require('passport');

var User      = require('./users');
//var Profile      = require('./profile');

var user = new User();


module.exports.register = function(req,res){

  user.username = req.body.username;
  user.email    = req.body.email;

  //console.log('setting password : '+ req.body.password);

  user.setPassword(req.body.password);
  //console.log("register hash : "+user.hash+"\n");
  //console.log("register salt : "+user.salt+"\n");

  user.save( function(err){
    console.log('call Register');
    var token = user.generateJwt();
    res.status(200);
    res.json({
      "username":user.username,
      "email":user.email,
      "hash":user.hash,
      "salt":user.salt,
      "token":token
    });
  })
};


module.exports.login = function(req,res){

  passport.authenticate( 'local', function(err, user, info){
    var token;

    if(err){
      res.status(404).json(err);
      return;
    }

    if(user){
      console.log('call login');
      token = user.generateJwt();
      res.status(200);
      res.json({
        "token": token, "user" : req.body.username
      });
    }else{
      //res.status(401).json(info);
      res.redirect('./login');

    }
  })(req,res);
};

module.exports.profileRead = function(req,res){
  if(!req.payload._id){
    res.status(401).json( {
      "message": "UnauthorizedError: private profile"
    });
  }else{
    User
    .findById(req.payload._id)
    .exec(function(err,user){
      res.status(200).json(user);
    })

  }
};


/*
 |--------------------------------------------------------------------------
 | Login Required Middleware
 |--------------------------------------------------------------------------
 */
module.exports.ensureAuthenticated = function(req, res, next) {
  if (!req.header('Authorization')) {
    return res.status(401).send({ message: 'Please make sure your request has an Authorization header' });
  }
  var token = req.header('Authorization').split(' ')[1];

  var payload = null;
  try {
    payload = jwt.decode(token, config.TOKEN_SECRET);
  }
  catch (err) {
    return res.status(401).send({ message: err.message });
  }

  if (payload.exp <= moment().unix()) {
    return res.status(401).send({ message: 'Token has expired' });
  }
  req.user = payload.sub;
  next();
}
