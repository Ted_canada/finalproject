var passport      = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var mongoose      = require('mongoose');


var User = require('./users');


passport.use( new LocalStrategy({
  usernameField:'username'
  },
  function(username, password, done){
    console.log('Passport.use');
    User.findOne({username:username}, function(err, user){
      if(err) { return err; }
      if(!user){
        return done(null,false, {message:'User not found'
        });
      }
      if(!user.validPassword(password)){
        return done(null,false, {message:'Password is wrong'
        });
      }
      return done(null, user);
    });
  }

));
