var express     = require('express');
var app         = express();
var bodyParser  = require('body-parser');
var path        = require('path');
var cors        = require('cors');
var passport    = require('passport');
var request     = require('request');
var moment      = require('moment');
var jwt         = require('jwt-simple');
var nodemailer  = require('nodemailer');




require('./app/model/passport');

app.use( bodyParser.urlencoded({extended:true}));
app.use( bodyParser.json());


app.use( express.static(path.join('/home/ubuntu/workspace/finalproject')));
app.use( express.static(path.join('/home/ubuntu/workspace/finalproject/src/app/')));
app.use( express.static(path.join('/home/ubuntu/workspace/finalproject/src')));
app.use( express.static(path.join('/home/ubuntu/workspace/finalproject/src/app/profile')));
app.use( express.static(path.join('/home/ubuntu/workspace/finalproject/src/app/profile-form')));
app.use( express.static(path.join('/home/ubuntu/workspace/finalproject/src/app/login')));
app.use( express.static(path.join('/home/ubuntu/workspace/finalproject/src/app/register')));


app.use(cors());
app.use(passport.initialize());

app.get('/', function(req,res){
  res.sendFile( path.join('/home/ubuntu/workspace/finalproject/src/app/index.html') );
});

// Back-end server setting
var port  = process.env.PORT || 8080;
var router = express.Router();
var mongoose    = require('mongoose');
mongoose.connect('mongodb://localhost:27017/profiles',function(err,db){
	console.log('Connected to DB');
	if (err) return console.log(err);
});


// external codes
var Profile       = require('./app/model/profile');
var GamesUserDB   = require('./app/model/gameDB');
var GameDB        = require('./app/model/gameDB');
var PlatformDB    = require('./app/model/gameDB');
var LfgDB        = require('./app/model/gameDB');
var CtrlAuth      = require('./app/model/authentication');
var User          = require('./app/model/users');
var config        = require('./app/model/auth-config');


router.use( function(err, req, res, next){
    console.log('Something is happening');
    next();
});

router.get('/', function(req, res){
	res.sendFile( '/home/ubuntu/workspace/finalproject/src/app/app.component.html');
	res.json({message:'welcome to our api'});

	console.log('Router GET');
});

// creating profiles
router.route('/profiles')
    .post( function( req, res ){
    	console.log('Profiles POST');
    	var profile  = new Profile();
      profile.username	 = req.body.username;
      profile.password	 = req.body.password;
    	profile.firstname	 = req.body.firstname;
      profile.lastname	 = req.body.lastname;
    	profile.age 		   = req.body.age;
    	profile.gended		 = req.body.gended;
    	profile.email	   	 = req.body.email;
    	profile.picture		  = req.body.picture;


      console.log(profile.username);
      console.log(profile.password);
    	console.log(profile.firstname);
      console.log(profile.lastname);
    	console.log(profile.age);
    	console.log(profile.gended);
    	console.log(profile.email);
    	console.log(profile.picture);


    	profile.save(function(err){
    		if(err){
    			return res.send(err);
    		}else{
          //console.log('profile ID :'+profile._id);
          CtrlAuth.register(req,res);
        }

    		//res.json( {message:'Profile created', data:profile});
      });
    })

    .get(function(req, res){
    	console.log('Profiles GET');
    	Profile.find(function(err,profiles){
    		if(err) {
          return res.send();
        }
    		res.json(profiles);
    	})
    });

   


// on routes that end in /profiles/:profile_id
// ----------------------------------------------------
router.route('/profiles/:profile_id')

	// get the profile with that id
	.get(function(req, res) {
    /*
		Profile.findById(req.params.profile_id, function(err, profile) {
			if (err){
				return res.send(err);
			}else{
          CtrlAuth.profileRead();
      }

			//res.json(profile);
		});*/
    if(!req.param.profile_id){
      res.status(401).json( {
        "message": "UnauthorizedError: private profile"
      });
    }else{
      User
      .findById(req.payload._id)
      .exec(function(err,user){
        res.status(200).json(user);
      })

    }
	})

  .post(function(req, res) {
    console.log('call put for certain User' + req.body.age);
    Profile.findById(req.params.profile_id, function(err, profile) {


      var ori_age = profile.age, ori_playID = profile.playerID;
      //console.log('ori_age ; ' + ori_age);
      //console.log('ori_playID ; ' + ori_playID);
      if (err){
        return res.send(err);
      }
      if( req.body.age != ''){
        //console.log('debug1');
        profile.age 		= req.body.age;
      }else{
        //console.log('debug2');
        profile.age     = ori_age;
      }

      if( req.body.playerID != '' ){
        profile.playerID 		= req.body.playerID;
      }else{
        profile.playerID    = ori_playID;
      }

      profile.save(function(err) {
        if (err){
          return res.send(err);
        }

        res.json({ message: 'age updated!' });
      });

    });
  })

	// update the bear with this id
	.put(function(req, res) {
		console.log(req.body);
		Profile.findById(req.params.profile_id, function(err, profile) {

			if (err){
				return res.send(err);
      }

      profile.username	= req.body.username;
      profile.password	= req.body.password;
			profile.firstname	= req.body.firstname;
      profile.lastname	= req.body.lastname;
			profile.age 		= req.body.age;
			profile.gended		= req.body.gended;
			profile.email		= req.body.email;
			profile.picture		= req.body.picture;
			profile.save(function(err) {
				if (err){
					return res.send(err);
        }

				res.json({ message: 'profile updated!' });
			});

		});
	})

	// delete the profile with this id
	.delete(function(req, res) {
		Profile.remove({
			_id: req.params.profile_id
		}, function(err, bear) {
			if (err){
				return res.send(err);
			}

			res.json({ message: 'Successfully deleted' });
		});
	});


///////////////////////////////////////////////////////
//// Authentication
///////////////////////////////////////////////////////
// routing for login service
router.route('/login')
  .post( function(req,res){
    console.log('request Post for login');

    CtrlAuth.login(req,res);
  });

///////////////////////////////////////////////////////
//// Game DB
///////////////////////////////////////////////////////
// routing for post for LFG pages
router.route('/games/lfg')
  .post( function(req,res){
    //game_item = new GameDB();
    lfg_item = new LfgDB();
    //lfg_item = game_item.LFG;

    var game_name        = req.body.gameName;

    console.log(' req.body.teamname : '+ req.body.LFGTeamName );
    console.log(' req.body.gameName : '+ req.body.lfgCreater );
    console.log(' req.body.gameName : '+ req.body.gameName );
    console.log(' req.body.gameName : '+ req.body.playStyle );
    console.log(' req.body.playertime  : '+ req.body.playertime  );
    console.log(' req.body.describe  : '+ req.body.describe  );


    if( req.body.playStyle != null && game_name != null ){
      console.log('lfg post');
      //gamer.username    = req.body.username;
      lfg_item.LFGTeamName  = req.body.LFGTeamName;
      lfg_item.lfgCreater  = req.body.lfgCreater;
      lfg_item.playStyle  = req.body.playStyle;
      lfg_item.playtime  = req.body.playertime;
      lfg_item.Description  = req.body.describe;
      lfg_item.gameName  = req.body.gameName;
      lfg_item.platformName  = req.body.platformName;



       GameDB.findOne( {gameName:game_name}, function(err,doc){
          if(err){
            console.log('Error finding game name from DB');
            return res.send(err);
          }
          doc.LFG.push({"LFGTeamName": lfg_item.LFGTeamName,
                      "LFGCreater": lfg_item.lfgCreater,
                      "playStyle": lfg_item.playStyle,
                      "playtime": lfg_item.playtime,
                      "Description": lfg_item.Description,
                      "gameName": lfg_item.gameName,
                      "platformName": lfg_item.platformName,
                    });

          doc.save();
          })
      res.json({ message: 'Successfully Post for LFG' });
      }

  });

  // routing for retrieving only LFG data
  router.route('/games/lfg/detail')
  .post( function(req,res){
    console.log('request Get for LFG');

    game_name = req.body.gameName;
    GameDB.findOne( {gameName:game_name}, function(err,doc){
      if(err) {
        return res.send();
      }
      console.log('return value : ' + doc);
      res.json({"lfgData": doc.LFG});
    })
  });

  // routing for saving platform information 
  router.route('/games/platform')
    .post( function(req,res){
      game_item = new GameDB();
      platform = new PlatformDB();

      var game_name        = req.body.gameName;



      ///// platform

      if( req.body.platform_item != null && req.body.gameName != null ){
        console.log("platform posting")
        console.log('platform item : '+ "["+game_name+"]"+req.body.platform_item);


         GameDB.findOne( {gameName:game_name}, function(err,doc){
            if(err){
              console.log('finding the game list from DB for platform');
              return res.send(err);
            }

            doc.Platforms.push({"platform_item":req.body.platform_item});
            //doc.Platforms.push({"usingMIC":platform.usingMIC});

            doc.save();
            })
        res.json({ message: 'Successfully Post for Platform' });
        }

    });

    // routing for getting platform information from DB
    router.route('/games/getplatform')
    .post( function(req,res){
      console.log('request Get for platform');

      game_name = req.body.gameName;
      GameDB.findOne( {gameName:game_name}, function(err,doc){
        if(err) {
          return res.send();
        }
        console.log('return value : ' + doc);
        res.json({"platform": doc.Platforms});
      })
    });



// routing for saving new game to DB
router.route('/games')
  .post( function(req,res){
    game_item = new GameDB();
    gamer = new GamesUserDB();

    var game_name        = req.body.gameName;

    console.log(' req.body.gameName : '+ req.body.gameName );
    console.log(' game_item.gameID  : '+ req.body.gameID  );
    if( req.body.gameName != null && req.body.gameID != null){
        console.log('debug1');
        game_item.gameID          = req.body.gameID;
        game_item.gameName        = req.body.gameName;
        game_item.gameRefImgPath  = req.body.gameRefImgPath;
        game_item.gameBgImg       = req.body.gameBgImg;
        game_item.gameTitle       = req.body.gameTitle;

        game_item.save(function(err) {
          if (err){
            return res.send(err);
          }

          res.json({ message: 'Successfully Post for game' });
        });
      }

    //// GameUserDB
    if( req.body.playerID != null && req.body.gameName != null ){
      console.log('game list posting');
      //gamer.username    = req.body.username;
      gamer.userPlayerID  = req.body.playerID;

    /*  gamer.save( function(err){
        if(err){
          console.log('game list save fail for DB');
          return res.send(err);
        }
      })*/

       GameDB.findOne( {gameName:game_name}, function(err,doc){
          if(err){
            console.log('fail to find game list from DB');
            return res.send(err);
          }

          doc.GameUsers.push({"userPlayerID":gamer.userPlayerID});

          doc.save();
          })
      res.json({ message: 'Successfully Post for Gamelist' });
    }


  })
  .get( function(req,res){
    console.log('request Get for game');

    GameDB.find(function(err,game_item){
      if(err) {
        return res.send();
      }
      res.json(game_item);
    })
  });

  router.route('/games/:game_item_id')
  .delete(function(req, res) {
    GameDB.remove({
      _id: req.params.game_item_id
    }, function(err, bear) {
      if (err){
        return res.send(err);
      }

      res.json({ message: 'Successfully deleted' });
    });
  });

///////////////////////////////////////////////////////
//// Email verification
///////////////////////////////////////////////////////

var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
      user: 'ckyuhyun@gmail.com',
      pass: 'rBgUsL4831!)ge'
  }
});

var rand, link, mailOptions;
var forwarding_res;
var host;

// send email to indivisual email account to verify
router.route('/sendmail')
  .post(function (req, res) {
    console.log('Receive a request for Email verification');
    host = req.get('host');
    rand=Math.floor((Math.random() * 100) + 54);
    link="http://"+req.get('host')+"/api/verify?id="+rand;
    forwarding_res = res;

    console.log('email addr : '+ req.body.emailaddr);
    mailOptions = {
        from: 'ckyuhyun@gmail.com',
        to: req.body.emailaddr,
        subject: 'Test Mailer',
        html: "Hello,<br> Please Click on the link to verify your email.<br><a href="+link+">Click here to verify</a>"

    };
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log('[Error] '+error);
        }

        console.log('Message %s sent: %s', info.messageId, info.response);
    });
  });

  // verify from indivisual email account
  router.route('/verify')
  .get(function(req,res){
      console.log('req.protocol : ' + req.protocol+"://"+req.get('host') );
      console.log('host : '+ ("http://"+host) );
      if((req.protocol+"://"+req.get('host'))==("http://"+host))
      {
          console.log("Domain is matched. Information is from Authentic email");
          if(req.query.id==rand)
          {
              console.log("email is verified");
              res.end("<h1>Email "+mailOptions.to+" is been Successfully verified");
              forwarding_res.send({"status": true});
          }
          else
          {
              console.log("email is not verified");
              res.end("<h1>Bad Request</h1>");
          }
      }
      else
      {
          res.end("<h1>Request is from unknown source");
      }

    });



/*
 |--------------------------------------------------------------------------
 | Generate JSON Web Token
 |--------------------------------------------------------------------------
 */
function createJWT(user) {
  var payload = {
    sub: user._id,
    iat: moment().unix(),
    exp: moment().add(14, 'days').unix()
  };
  return jwt.encode(payload, config.TOKEN_SECRET);
}

/*
 |--------------------------------------------------------------------------
 | Login with Google
 |--------------------------------------------------------------------------
 */
router.route('/auth/google')
.post(function(req, res) {
  console.log('Call auth for google');
  var accessTokenUrl = 'https://www.googleapis.com/oauth2/v4/token';
  //var accessTokenUrl = 'https://www.accounts.google.com/o/oauth2/token';
  var peopleApiUrl = 'https://www.googleapis.com/oauth2/v2/userinfo?fields=email%2Cfamily_name%2Cgender%2Cgiven_name%2Chd%2Cid%2Clink%2Clocale%2Cname%2Cpicture%2Cverified_email';
  var params = {
    code: req.body.code,
    client_id: req.body.clientId,
    client_secret: config.GOOGLE_SECRET,
    redirect_uri: req.body.redirectUri,
    grant_type: 'authorization_code'
  };

  console.log('req.body : ' +req.body);
  console.log('code : ' +req.body.code);
  console.log('clientId : ' +req.body.clientId);
  console.log(config.GOOGLE_SECRET);
  console.log(req.body.redirectUri);

   var token_request='code='+req.body.code+
        '&client_id='+req.body.clientId+
        '&client_secret='+config.GOOGLE_SECRET+
        '&redirect_uri='+req.body.redirectUri+
        '&grant_type=authorization_code';
    var request_length = token_request.length;

    console.log('\ntoken_request :'+ token_request);
  // Step 1. Exchange authorization code for access token.
  request.post(accessTokenUrl, { body: token_request, headers: {'Content-type':'application/x-www-form-urlencoded'} }, function(err, response, token) {
    var accessToken = JSON.parse(token).access_token;
    var headers = { Authorization: 'Bearer ' + accessToken };

    console.log( 'token :'+ token);
    console.log('accessToken :'+ accessToken);
    console.log('\n peopleApiUrl :'+ peopleApiUrl);


    // Step 2. Retrieve profile information about the current user.
    request.get({ url: peopleApiUrl, headers: headers, json: true }, function(err, response, profile) {
      console.log('profile.email : '+ profile.email);
      console.log('profile.id : '+ profile.id);
      console.log('profile.name : '+ profile.name);

      if (profile.error) {
        console.log('profile :'+profile);
        console.log('profile.error :'+profile.error);
        return res.status(500).send({message: profile.error.message});
      }

      User.findOne({ email: profile.email }, function(err, existingUser) {
          if (existingUser && existingUser.provider == "google") {
            var token = createJWT(existingUser);
            //res.send({ token: token });

            res.send(
              { "token": token , "user" : profile.name});
          }
          else if (existingUser && existingUser.provider != "google") {
            console.log('......Debug3');
            var user = {};
              user.provider_id = profile.id;
              user.provider = "google";
              user.email = profile.email;
              user.picture = profile.picture.replace('sz=50', 'sz=200');
              user.displayName = profile.name;
              User.findOneAndUpdate({email:existingUser.email},user, function(err) {
                var token = createJWT(existingUser);
                res.send(
                  { "token": token , "user" : profile.name});
              });
          }
          else{
              var user = new User();
              user.provider_id = profile.id;
              user.provider = "google";
              user.email = profile.email;
              user.picture = profile.picture.replace('sz=50', 'sz=200');
              user.displayName = profile.name;
              user.save(function(err) {
                var token = createJWT(user);
                res.send({ token: token });
              });
          }
         // var token = req.header('Authorization').split(' ')[1];
         // var payload = jwt.decode(token, config.TOKEN_SECRET);
        });
    });
  });
});





app.use('/api', router);

app.listen(port, function(){
	console.log('magic happends on port' + port);
});
